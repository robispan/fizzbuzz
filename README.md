##FizzBuzz game

- User chooses a number from 1 to 100,
- game Displays integers from 1 to chosen number in lines,
- replaces numbers that are divisible by 3 with "fizz" 
- replaces numbers divisible by 5 with "buzz"
- replaces numbers divisible by 3 and 5 with "fizzbuzz"