print('Welcome to the fizzbuzz game!')

number = int(input('Enter a number between 1 and 100: '))
while number > 100 or number < 1:
    number = int(input('Your number was too high or too low. Enter a number between 1 and 100: '))

for i in range(1, number):
    if i % 3 == 0 and i % 5 != 0:
        print('fizz')
    elif i % 5 == 0 and i % 3 != 0:
        print('buzz')
    elif i % 3 == 0 and i % 5 == 0:
        print('fizzbuzz')
    else:
        print(i)

input('')
